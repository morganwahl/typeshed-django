from django.db.models import Model


class AbstractUser(Model):
    class Meta:
        abstract = True
