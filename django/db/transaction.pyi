from contextlib import ContextDecorator
from typing import Callable, Union, Any


class Atomic(ContextDecorator):
    def __init__(self, using: str, savepoint: str) -> None: ...


def atomic() -> Atomic: ...
