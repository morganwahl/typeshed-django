from typing import Type, Union


class Field(object):
    ...


class TextField(Field):
    ...


class ManyToMany(Field):
    def __init__(
        self,
        related_model: Union[Type['Model'], str],
        related_name: str,
    ) -> None:
        ...


class Model(object):
    ...
